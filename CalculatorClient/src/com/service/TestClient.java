package com.service;

import java.rmi.RemoteException;

public class TestClient {

	public static void main(String[] args) throws RemoteException {
		// TODO Auto-generated method stub

		CalculateStub stub = new CalculateStub();
		CalculateStub.Add param = new CalculateStub.Add();
		param.setA(3);
		param.setB(6);
		CalculateStub.AddResponse response = stub.add(param);
		int result = response.get_return();
		System.out.println("Result" + result);
	}

}
